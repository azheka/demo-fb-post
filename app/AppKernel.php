<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    
    public function getName()
    {
        return parent::getName();
    }
    
    public function getCharset()
    {
        return 'UTF-8';
    }
    
    public function getLogDir()
    {
        return __DIR__ .'/../data/logs/'. $this->getEnvironment();
    }
    
    public function getCacheDir()
    {
        return __DIR__ .'/../data/cache/'. $this->getEnvironment();
    }
    
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
        );
        
        $search = realpath(__DIR__ .'/../src');
        $finder = new Symfony\Component\Finder\Finder();
        $finder->files()->in($search)->name('*Bundle.php');
        foreach ($finder as $file) {
            $path       = substr($file->getRealpath(), strlen($search) + 1, -4);
            $parts      = explode('/', $path);
            $class      = array_pop($parts);
            $namespace  = implode('\\', $parts);
            $class      = $namespace .'\\'. $class;
        
            $bundles[]  = new $class();
        }
        
        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }
        
        
        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }
}
