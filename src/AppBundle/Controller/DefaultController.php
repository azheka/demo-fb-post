<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Entity;

class DefaultController extends Controller
{

    public function indexAction(Request $request)
    {
        $userId = $this->container->get("session")->get("facebook_userId");
        if(!$userId) {
            return $this->render('AppBundle:default:index.html.twig', array(
                'access' => false,
            ));
        }
        
        $entity = new Entity\Post();
        
        $service = $this->container->get('app.service.facebook');
        
        $form = $this->createForm('app_form_request', $entity, array(
            'action' => $request->getUri(),
            'method' => 'POST'
        ));
        
        $form->handleRequest($request);
        if ($request->isMethod('POST')) {
            if ($form->isValid()) {
                if($service->addPost($userId, $entity)) {
                    $this->addFlash('notice', 'Thank!');
                    return $this->redirect('/');
                }
            }
        }
        
        $pagination = $service->getPostPagination($userId, $request->query->get('page', 1), 10);
        return $this->render('AppBundle:default:index.html.twig', array(
            'access'     => true,
            'form'       => $form->createView(),
            'pagination' => $pagination
        ));
    }
    
    public function loginAction(Request $request)
    {
        $uri = $this->container->get('app.service.facebook')->getLoginUrl();
        return $this->redirect($uri);
    }

    public function sessionAction(Request $request)
    {
        $service = $this->container->get('app.service.facebook');
        if(!$service->updateAccess()) {
            $this->addFlash('error', 'Something happened!');
        }
        
        return $this->redirect('/');
    }
}
