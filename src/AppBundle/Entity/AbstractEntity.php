<?php
namespace AppBundle\Entity;

use Doctrine\Common\Proxy\Exception\InvalidArgumentException;

abstract class AbstractEntity
{

    public function __construct(array $params = array())
    {
        $this->exchange($params);
    }
    
    public function exchange(array $params = array())
    {
        if($params) {
            foreach($params as $propertie => $value) {
                $method = sprintf('set%s', ucfirst($propertie));
                $this->$method($value);
            }
        }
        
        return $this;
    }
    
    public function __set($name, $value)
    {
        $method = 'set'.ucfirst($name);
        call_user_func(array($this, $method), $value);
        return $this;
    }
    
    public function __get($property)
    {
        if(! property_exists($this, $property)) {
            throw new InvalidArgumentException("Property {$property} doesn't exist");
        }
        
        return $this->$property;
    }

    public function __call($name, $params)
    {
        if(strpos($name, 'has') === 0) {
            $name = lcfirst(substr($name, 3));
            return property_exists($this, $name);
        }
        
        if(strpos($name, 'is') === 0) {
            $name = lcfirst(substr($name, 2));
            return property_exists($this, $name);
        }
        
        if(strpos($name, 'set') === 0) {
            $name = lcfirst(substr($name, 3));
            if(property_exists($this, $name)) {
                $this->$name = array_pop($params);
                return $this;
            }
        }
        
        if(strpos($name, 'get') === 0) {
            $name = lcfirst(substr($name, 3));
            if(property_exists($this, $name)) {
                return $this->$name;
            }
            else {
                return null;
            }
        }
        
        return false;
    }

    public function toArray()
    {
        // Now get our reflection class for this class name
        $reflection = new \ReflectionClass($this);
        $class = get_class($this);
        
        // Then grap the class properites
        $properties = $reflection->getProperties();
        if($this instanceof \Doctrine\ORM\Proxy\Proxy) {
            $parent = $reflection->getParentClass();
            $class = $parent->getName();
            $properties = $parent->getProperties();
        }
        
        // A new array to hold things for us
        $toArray = array();
        
        // Lets loop through those class properties now
        foreach($properties as $prop) {
            // We know the property, lets craft a getProperty method
            $method = sprintf('get%s', ucfirst($prop->name));
            
            // It did, so lets call it!
            $value = $this->$method();
            if ($value instanceof \DateTime) {
                $value = $value->format('Y-m-d H:i:s');
            }
            else if($value instanceof self) {
                $value = $value->toArray();
            }
            else if($value instanceof \Doctrine\ORM\PersistentCollection) {
                continue;
            }
            
            $toArray[$prop->name] = $value;
        }
        
        // All done, send it back!
        return $toArray;
    }
}
