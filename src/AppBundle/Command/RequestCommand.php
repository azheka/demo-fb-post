<?php 
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class RequestCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('request:report')->setDescription('Build CSV report');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Start:');
        
        $service = $this->getContainer()->get('app.service.facebook');
        $reposition = $service->getAccess();
        foreach($reposition->findAll() as $access) {
            $session = new \Facebook\FacebookSession($access->getToken());
            
            $posted = $access->getPosts();
            foreach($posted as $posted) {
                $request = new \Facebook\FacebookRequest($session, 'POST', '/me/feed', array ('message' => $posted->getContent()));
                $response = $request->execute();
                $graph = $response->getGraphObject();
                
                $service->setPosted($posted);
            }
        }
        
        $output->writeln('End runing');
    }
}